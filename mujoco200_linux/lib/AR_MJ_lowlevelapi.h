

#define NUM_MOTORS 20
#define NUM_JOINTS 10


typedef struct {
  // All values are in SI units (N-m, rad, rad/s, V). Temperature is in deg C

  // Time since control program started
  double time;

  // Estimated pose and velocity of base frame
  struct {
    double translation[3];
    // Orientation stored Eigen-style, w component last
    double orientation[4];
    double linear_velocity[3];
    double angular_velocity[3];
  } base;

  // Raw sensor signals from IMU
  struct {
    // Orientation stored Eigen-style, w component last
    double orientation[4];
    double angular_velocity[3];
    double linear_acceleration[3];
    double magnetic_field[3];
  } imu;

  // Actuated joints
  struct {
    double position[NUM_MOTORS];
    double velocity[NUM_MOTORS];
    double torque[NUM_MOTORS];
  } motor;

  // Unactuated joints
  struct {
    double position[NUM_JOINTS];
    double velocity[NUM_JOINTS];
  } joint;

  struct {
    double com_pos[3];
  } robot;

  // Expressed as percent (0-100)
  int16_t battery_charge;

} llapi_observation_t;
//int llapi_get_observation(llapi_observation_t* obs);

// Returns a pointer to a struct describing the maximum torque/damping command
// values for each actuated joint. Any commanded values with a magnitude
// greater than these limits will be clamped by the robot before being applied.
// The damping limit for each joint is set such that the maximum value is still
// stable, while a value of roughly 20% of the limit is a reasonable value in
// most situations. These values are obtained when the connection to the robot
// or simulator is established, and calling the function beforehand returns a
// null pointer.
typedef struct {
  double torque_limit[NUM_MOTORS];
  double damping_limit[NUM_MOTORS];
} llapi_limits_t;
//const llapi_limits_t* llapi_get_limits();

// Sends command to robot
// The motor drivers can track a commanded velocity with the specified damping
// (optional), and/or be controlled directly via the feed-forward torque
// command. Damping is in N-m / (rad/s).
typedef struct {
  double torque;
  double velocity;
  double damping;
} llapi_motor_t;
typedef struct {
  llapi_motor_t motors[NUM_MOTORS];
  int32_t fallback_opmode;
  bool apply_command;
} llapi_command_t;
//void llapi_send_command(llapi_command_t* cmd);

// Values from this enum can be used in place of numeric indices when indexing
// the motor fields in the observation and command arrays.
enum DigitMotors {
  LeftHipRoll,
  LeftHipYaw,
  LeftHipPitch,
  LeftKnee,
  LeftToeA,
  LeftToeB,

  RightHipRoll,
  RightHipYaw,
  RightHipPitch,
  RightKnee,
  RightToeA,
  RightToeB,

  LeftShoulderRoll,
  LeftShoulderPitch,
  LeftShoulderYaw,
  LeftElbow,

  RightShoulderRoll,
  RightShoulderPitch,
  RightShoulderYaw,
  RightElbow,
};

// Values from this enum can be used in place of numeric indices when indexing
// the joint fields in the observation and command arrays.
enum DigitJoints {
  LeftShin,
  LeftTarsus,
  LeftToePitch,
  LeftToeRoll,
  LeftHeelSpring,

  RightShin,
  RightTarsus,
  RightToePitch,
  RightToeRoll,
  RightHeelSpring,
};