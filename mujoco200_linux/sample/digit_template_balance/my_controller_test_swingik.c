double setpt[nact]={0};
int upper_body_motors[8]={LeftShoulderRoll,
                                    LeftShoulderPitch,
                                    LeftShoulderYaw,
                                    LeftElbow,

                                    RightShoulderRoll,
                                    RightShoulderPitch,
                                    RightShoulderYaw,
                                    RightElbow,};
double msd;
double tf = 3;
double t_ctr = 0;
char stance_leg ='r';
double th0[3][1]={0};

double theta0;
double theta1;
double theta2;
double theta3;
double theta4;
double Ltoe_pitch;
double Rtoe_pitch;
double tf;
double Lp0[4][1]={0};
double Lpf[4][1]={0};
double Vp0[3][1]={0};
double Vpf[3][1]={0};
double Ap0[3][1]={0};
double Apf[3][1]={0};
double Lp0_2[4][1]={0};
double ss[4][1]={0};

double vv[3][1]={0}; 
double aa[3][1]={0};
double base_H[4][4]={0};
bool conv1 = true; //convergence of ik solver
bool conv2 = true; //convergence of ik solver

//initialize the controller here
void init_controller()
{
  ss[3][0]=1;
  tf=3;
  int i;
  double target[]={0,0,0,0, //left leg
                   0,0,
                  0,0,0,0, //right leg
                  0,0,
                   0,0,0,0,  //left hand
                   0,0,0,0};  //right hand

  for (i=0;i<nact;i++)
  {
    setpt[i]=target[i];
  }
}

//control in this loop
void control_loop(const llapi_observation_t* observation, llapi_command_t* command)
{
  msd = observation->time;
  // printf("msd: %f \n",msd);
  // printf("tf: %f \n",tf);
  //printf("time: %f \n", msd);;
  /*theta0 = 0;
  theta1 = theta1_mo-observation->motor.position[LeftHipPitch];
  theta2 = theta1_mo+observation->motor.position[RightHipPitch];
  theta3 = theta2_mo+observation->motor.position[LeftKnee];
  theta4 = theta2_mo-observation->motor.position[RightKnee];*/

  //command->motors[LeftHipYaw].torque = -1000*(observation->motor.position[LeftHipYaw])-(10*observation->motor.velocity[LeftHipYaw]);
  //command->motors[RightHipYaw].torque = -1000*(observation->motor.position[RightHipYaw])-(10*observation->motor.velocity[RightHipYaw]);

  base_H[0][0]=d->xmat[mj_name2id(m, mjOBJ_XBODY, "base")*9];
  base_H[0][1]=d->xmat[mj_name2id(m, mjOBJ_XBODY, "base")*9+1];
  base_H[0][2]=d->xmat[mj_name2id(m, mjOBJ_XBODY, "base")*9+2];
  base_H[0][3]=d->xpos[mj_name2id(m, mjOBJ_XBODY, "base")*3];
  base_H[1][0]=d->xmat[mj_name2id(m, mjOBJ_XBODY, "base")*9+3];
  base_H[1][1]=d->xmat[mj_name2id(m, mjOBJ_XBODY, "base")*9+4];
  base_H[1][2]=d->xmat[mj_name2id(m, mjOBJ_XBODY, "base")*9+5];
  base_H[1][3]=d->xpos[mj_name2id(m, mjOBJ_XBODY, "base")*3+1];;
  base_H[2][0]=d->xmat[mj_name2id(m, mjOBJ_XBODY, "base")*9+6];
  base_H[2][1]=d->xmat[mj_name2id(m, mjOBJ_XBODY, "base")*9+7];
  base_H[2][2]=d->xmat[mj_name2id(m, mjOBJ_XBODY, "base")*9+8];
  base_H[2][3]=d->xpos[mj_name2id(m, mjOBJ_XBODY, "base")*3+2];
  base_H[3][0]=0;
  base_H[3][1]=0;
  base_H[3][2]=0;
  base_H[3][3]=1;
  matPrint(4,4,base_H);




  for (int i=0;i<8;i++)
  {
    command->motors[upper_body_motors[i]].torque = -100*(observation->motor.position[upper_body_motors[i]]+setpt[upper_body_motors[i]])-(1*observation->motor.velocity[upper_body_motors[i]]);

  }

  // d->qpos[base_joint_adr+0]=0;
  // d->qpos[base_joint_adr+1]=0;
  // d->qpos[base_joint_adr+2]=1.2;//1.08;//1.065;
  // d->qpos[base_joint_adr+3]=1;//0.9238795;//0.9659258;//0.9990482;//w
  // d->qpos[base_joint_adr+4]=0;//0.258819;//0.0864101;//0;
  // d->qpos[base_joint_adr+5]=0;//0;
  // d->qpos[base_joint_adr+6]=0;//-0.3826834;//0.1300295;//0.258819;//0.0436194;
  // d->qvel[base_joint_adr]=0;
  // d->qvel[base_joint_adr+1]=0;
  // d->qvel[base_joint_adr+2]=0;
 /* parallel_toe(&theta0, &theta1, &theta3, &Ltoe_pitch);
  parallel_toe(&theta0, &theta2, &theta4, &Rtoe_pitch);*/

/*  printf("base_x: %f \n",d->xpos[mj_name2id(m, mjOBJ_XBODY, "base")*3]);
  printf("base_y: %f \n",d->xpos[mj_name2id(m, mjOBJ_XBODY, "base")*3+1]);
  printf("base_z: %f \n",d->xpos[mj_name2id(m, mjOBJ_XBODY, "base")*3+2]);*/
  
  if (msd<tf){
    

    for (int i=0;i<12;i++)
  {
    command->motors[i].torque = -100*(observation->motor.position[i]+setpt[i])-(1*observation->motor.velocity[i]);

  }

  Lp0[0][0]=1*(d->xpos[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*3]);
  Lp0[1][0]=1*(d->xpos[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*3+1]);
  Lp0[2][0]=1*(d->xpos[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*3+2]);
  Lp0[3][0]=1;


  Lpf[0][0]=1*Lp0[0][0]+0.1;
  Lpf[1][0]=1*Lp0[0][1]-0.4;
  Lpf[2][0]=1*Lp0[0][2]+0.1;



  /*Vp0[0][0]=d->xvel[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*3];
  Vp0[1][0]=d->xvel[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*3+1];
  Vp0[2][0]=d->xvel[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*3+2];*/

  t_ctr=msd;
       

    }
    else{


      for (int i=0;i<8;i++)
  {
    command->motors[upper_body_motors[i]].torque =  - 10*(observation->motor.position[upper_body_motors[i]]+setpt[upper_body_motors[i]])-(1*observation->motor.velocity[upper_body_motors[i]]);
    // printf("Torques motor %d :%f\n",upper_body_motors[i],command->motors[upper_body_motors[i]].torque*gearing[upper_body_motors[i]]);
    // printf("qfrc motor %d :%f\n",v_id[upper_body_motors[i]],d->qfrc_bias[v_id[upper_body_motors[i]]]);//+d->qfrc_applied[v_id[upper_body_motors[i]]]);
  }


  //      for (int i=0;i<12;i++)
  // {
  //     command->motors[i].torque =0;
  //   }
      if(msd<=t_ctr+tf ){
        for(int i=0;i<3;i++){
        ctraj(&ss[i][0],&vv[i][0],&aa[i][0],msd,t_ctr,t_ctr+tf, Lp0[i][0], Lpf[i][0], Vp0[i][0], Vpf[i][0], Ap0[i][0], Apf[i][0]);
        Lp0_2[0][0]=1*(d->xpos[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*3]);
        Lp0_2[1][0]=1*(d->xpos[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*3+1]);
        Lp0_2[2][0]=1*(d->xpos[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*3+2]-1.2);
       // matPrint(3,1,Lp0_2);
      
      }}
      else if(msd>t_ctr+tf && msd <= t_ctr+2*tf ){
        //printf("2222222222222222222222\n");
        for(int i=0;i<3;i++){
        ctraj(&ss[i][0],&vv[i][0],&aa[i][0],msd,t_ctr+tf,t_ctr+(2*tf), Lpf[i][0], Lp0[i][0], Vp0[i][0], Vpf[i][0], Ap0[i][0], Apf[i][0]);
 
      }}
      // printf("ss_z %f \n",ss[2][0]);
      // printf("LTR_z %f \n",d->xpos[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*3+2]-1.2);

      double ss_input[4][1];
      double inv_base_H[4][4];
      matInv2(4,base_H,inv_base_H);
      matPrint(4,1,ss_input);
      matMult(4,4,4,1,inv_base_H,ss,ss_input);

      newtonik_SwingLeg(stance_leg,observation,ss_input,th0,conv1);
      if(conv1 == false){
        //safety_toggle=true;//Add safety measure incase IK solution does not converge
      }

      double th_Lfeet[2][1];
      newtonik_FlatFoot(stance_leg,d,m,th_Lfeet,conv2);
      if(conv2 == false){
        //safety_toggle=true;
      }




      //matPrint(3,1,th0);
      setpt[0]=th0[0][0];
      setpt[2]=th0[1][0];
      setpt[3]=th0[2][0];

      //setpt[4]=th_Lfeet[0][0];
      //setpt[5]=th_Lfeet[1][0];
      //d->qpos[m->jnt_qposadr[m->body_jntadr[mj_name2id(m, mjOBJ_BODY, "left-toe-pitch")]]] = th_Lfeet[0][0];
      //d->qpos[m->jnt_qposadr[m->body_jntadr[mj_name2id(m, mjOBJ_BODY, "left-toe-roll")]]] = th_Lfeet[1][0];

      

      // printf("setpt RHP: %f \n",setpt[2]);
      // printf("pos RHP: %f \n",observation->motor.position[2]);


      for (int i=0;i<12;i++)
  {
    command->motors[i].torque = 1000*(-observation->motor.position[i]+setpt[i])-(10*observation->motor.velocity[i]);

  }
  // command->motors[LeftToeA].torque = -10*(th_Lfeet[0][0]-observation->joint.position[LeftToePitch])-10*(th_Lfeet[1][0]+observation->joint.position[LeftToeRoll])-1*observation->joint.velocity[LeftToePitch]-1*observation->joint.velocity[LeftToeRoll];
  // command->motors[LeftToeB].torque = +10*(th_Lfeet[0][0]-observation->joint.position[LeftToePitch])-10*(th_Lfeet[1][0]+observation->joint.position[LeftToeRoll])+1*observation->joint.velocity[LeftToePitch]-1*observation->joint.velocity[LeftToeRoll];
  command->motors[LeftToeA].torque = -10*(th_Lfeet[0][0]-observation->joint.position[LeftToePitch])+1*observation->joint.velocity[LeftToePitch]+10*(th_Lfeet[1][0]-observation->joint.position[LeftToeRoll])-1*observation->joint.velocity[LeftToeRoll];
  command->motors[LeftToeB].torque = 10*(th_Lfeet[0][0]-observation->joint.position[LeftToePitch])-1*observation->joint.velocity[LeftToePitch]+10*(th_Lfeet[1][0]-observation->joint.position[LeftToeRoll])-1*observation->joint.velocity[LeftToeRoll];;


    
    }



  }

  

