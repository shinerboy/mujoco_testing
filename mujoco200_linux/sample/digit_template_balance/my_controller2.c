double setpt[nact]={0};
int upper_body_motors[8]={LeftShoulderRoll,
                                    LeftShoulderPitch,
                                    LeftShoulderYaw,
                                    LeftElbow,

                                    RightShoulderRoll,
                                    RightShoulderPitch,
                                    RightShoulderYaw,
                                    RightElbow,};
double msd;

double theta0;
double theta1;
double theta2;
double theta3;
double theta4;
double Ltoe_pitch;
double Rtoe_pitch;
double tf;

//initialize the controller here
void init_controller()
{
  tf=20;
  int i;
  double target[]={0.3752,0.2,0.3,-0.75, //left leg
                   0,0,
                  -0.3752,-0.2,-0.3,0.75, //right leg
                  0,0,
                   0,0,0,0,  //left hand
                   0,0,0,0};  //right hand

  for (i=0;i<nact;i++)
  {
    setpt[i]=target[i];
  }
}

//control in this loop
void control_loop(const llapi_observation_t* observation, llapi_command_t* command)
{
  msd = observation->time;
  //printf("time: %f \n", msd);;
  theta0 = 0;
  theta1 = theta1_mo-observation->motor.position[LeftHipPitch];
  theta2 = theta1_mo+observation->motor.position[RightHipPitch];
  theta3 = theta2_mo+observation->motor.position[LeftKnee];
  theta4 = theta2_mo-observation->motor.position[RightKnee];

  command->motors[LeftHipYaw].torque = -1000*(observation->motor.position[LeftHipYaw])-(10*observation->motor.velocity[LeftHipYaw]);
  command->motors[RightHipYaw].torque = -1000*(observation->motor.position[RightHipYaw])-(10*observation->motor.velocity[RightHipYaw]);


  for (int i=0;i<8;i++)
  {
    command->motors[upper_body_motors[i]].torque = -100*(observation->motor.position[upper_body_motors[i]]+setpt[upper_body_motors[i]])-(1*observation->motor.velocity[upper_body_motors[i]]);

  }

  if (msd<3*tf/5){
        //d->qpos[base_joint_adr]=0;
        //d->qpos[base_joint_adr+1]=0;
        d->qpos[base_joint_adr+2]=1.2;//1.08;//1.065;
        d->qpos[base_joint_adr+3]=0;//0.9238795;//0.9659258;//0.9990482;//w
        d->qpos[base_joint_adr+4]=0;//0.258819;//0.0864101;//0;
        d->qpos[base_joint_adr+5]=0;//0;
        d->qpos[base_joint_adr+6]=1;//-0.3826834;//0.1300295;//0.258819;//0.0436194;
        d->qvel[base_joint_adr]=0;
        d->qvel[base_joint_adr+1]=0;
        d->qvel[base_joint_adr+2]=0;
        //d->qvel[base_joint_adr+3]=0;
        //d->qvel[base_joint_adr+4]=0;
        //d->qvel[base_joint_adr+5]=0;
       // d->qpos[body_x_joint_adr] = 0.0;
        //d->qpos[body_pitch_joint_adr] = 0.0;


        //d->qpos[body_z_joint_adr] = -0.92;
        //d->qpos[body_x_joint_adr] = 0.0;
        //d->qpos[body_pitch_joint_adr] = -0.01;//-0.01
        //printf("--------------------------------------------------------------------------------------");
        parallel_toe(&theta0, &theta1, &theta3, &Ltoe_pitch);
        parallel_toe(&theta0, &theta2, &theta4, &Rtoe_pitch);

    }




   if(msd<tf){

        command->motors[LeftHipRoll].torque = -1000*(observation->motor.position[LeftHipRoll]-20*M_PI/180)-(10*observation->motor.velocity[LeftHipRoll]);
        command->motors[RightHipRoll].torque = -1000*(observation->motor.position[RightHipRoll]+20*M_PI/180)-(10*observation->motor.velocity[RightHipRoll]);

        command->motors[LeftHipPitch].torque = -1000*(observation->motor.position[LeftHipPitch]-(theta1_mo-mid1_angle1))-(10*observation->motor.velocity[LeftHipPitch]);
        command->motors[LeftKnee].torque = -1000*(observation->motor.position[LeftKnee]-(-theta2_mo+mid1_angle2))-(10*observation->motor.velocity[LeftKnee]);

        command->motors[RightHipPitch].torque = -1000*(observation->motor.position[RightHipPitch]+(theta1_mo-mid1_angle1))-(10*observation->motor.velocity[RightHipPitch]);
        command->motors[RightKnee].torque = -1000*(observation->motor.position[RightKnee]+(-theta2_mo+mid1_angle2))-(10*observation->motor.velocity[RightKnee]);
        
        

        command->motors[RightToeA].torque = 50*(observation->joint.position[RightToePitch]+Rtoe_pitch);
        command->motors[RightToeB].torque = -50*(observation->joint.position[RightToePitch]+Rtoe_pitch);
        command->motors[LeftToeA].torque = 50*(observation->joint.position[LeftToePitch]-Ltoe_pitch);
        command->motors[LeftToeB].torque = -50*(observation->joint.position[LeftToePitch]-Ltoe_pitch);

      if(msd>=3*tf/5 && msd <4*tf/5){
        

        //d->qpos[base_joint_adr+2]=1.08;//1.065;
        d->qpos[base_joint_adr+3]=0;//0.9238795;//0.9659258;//0.9990482;//w
        d->qpos[base_joint_adr+4]=0;//0.258819;//0.0864101;//0;
        d->qpos[base_joint_adr+5]=0;//0;
        d->qpos[base_joint_adr+6]=1;//-0.3826834;//0.1300295;//0.258819;//0.0436194;
        d->qvel[base_joint_adr]=0;
        d->qvel[base_joint_adr+1]=0;

      }
    }

    else if(msd>=tf){

      command->motors[LeftHipRoll].torque = -1000*(observation->motor.position[LeftHipRoll]-20*M_PI/180)-(10*observation->motor.velocity[LeftHipRoll]);
      command->motors[RightHipRoll].torque = -1000*(observation->motor.position[RightHipRoll]+20*M_PI/180)-(10*observation->motor.velocity[RightHipRoll]);

      command->motors[LeftHipPitch].torque = -1000*(observation->motor.position[LeftHipPitch]-(theta1_mo-mid1_angle1))-(10*observation->motor.velocity[LeftHipPitch]);
      command->motors[LeftKnee].torque = -1000*(observation->motor.position[LeftKnee]-(-theta2_mo+mid1_angle2))-(10*observation->motor.velocity[LeftKnee]);

      command->motors[RightHipPitch].torque = -1000*(observation->motor.position[RightHipPitch]+(theta1_mo-mid1_angle1))-(10*observation->motor.velocity[RightHipPitch]);
      command->motors[RightKnee].torque = -1000*(observation->motor.position[RightKnee]+(-theta2_mo+mid1_angle2))-(10*observation->motor.velocity[RightKnee]);
        
      

      command->motors[LeftToeA].torque = ((10*(dist_com[0][0]))+1.2*(base_angvel2[1][0])-8.0*(0+observation->joint.position[LeftToeRoll]) - 1*observation->joint.velocity[LeftToeRoll]);
      command->motors[LeftToeB].torque = ((-10*(dist_com[0][0]))-1.2*(base_angvel2[1][0])+8.0*(0-observation->joint.position[LeftToeRoll]) - 1*observation->joint.velocity[LeftToeRoll]);

      command->motors[RightToeA].torque = ((-10*(dist_com[0][0]))-1.2*(base_angvel2[1][0])-8.0*(-0+observation->joint.position[RightToeRoll]) - 1*observation->joint.velocity[RightToeRoll]);
      command->motors[RightToeB].torque = ((10*(dist_com[0][0]))+1.2*(base_angvel2[1][0])+8.0*(-0-observation->joint.position[RightToeRoll]) - 1*observation->joint.velocity[RightToeRoll]);

  }

  }

  

