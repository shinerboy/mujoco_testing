//  USE THIS TO SET INITIAL CONDITIONS/PARAMETERS IN MUJOCO

double *params;

//Used for get phase angle method
double L; //desired leg length.
double step_angle; //desired step angle

double mid1_angle1; //Midstance Hip Pitch of stance leg
double mid1_angle2; //Midstance Knee of stance leg
double fs1_angle1; //Foot strike Hip pitch of trailing leg
double fs1_angle2; //Foot strike Knee of trailing leg
double mid2_angle1; //Midstance Hip Pitch of swing leg
double mid2_angle2; //Midstance knee of swing leg
double fs2_angle1;  //Foot strike Hip Pitch of leading leg
double fs2_angle2;  //Foot strike knee of leading leg

double theta1_mo = +45*(M_PI/180); //Add this to sensor reading to get angle in model frame. Subtract to desired model angle to get mujoco angle
double theta2_mo = -82.25*(M_PI/180); //Add this to sensor reading to get angle in model frame. Subtract to desired model angle to get mujoco angle



void init_setup()
{

}

void setup()
{
  d->qpos[base_joint_adr]=0;
  d->qpos[base_joint_adr+1]=0;
  d->qpos[base_joint_adr+2]=1.1;

  d->qpos[q_id[LeftHipRoll]]=20.0/180.0*mjPI; //LHR
  d->qpos[q_id[RightHipRoll]]=-20.0/180.0*mjPI; //RHR

  params = (double*)malloc(sizeof(double)*27);
  get_params(params);

  L=0.9;
  step_angle=0*M_PI/180;
  getphaseangles(&mid1_angle1, &mid1_angle2, params, &L, &step_angle);

  L=0.9;
  step_angle=0*M_PI/180;
  getphaseangles(&mid2_angle1, &mid2_angle2, params, &L, &step_angle);

  L=0.9;
  step_angle=25*M_PI/180;
  getphaseangles(&fs1_angle1, &fs1_angle2, params, &L, &step_angle);

  L=0.7;
  step_angle=0*M_PI/180;
  getphaseangles(&fs2_angle1, &fs2_angle2, params, &L, &step_angle);

  d->qpos[q_id[LeftHipPitch]]=theta1_mo-mid1_angle1;//30.0/180.0*mjPI; //LHP
  d->qpos[q_id[LeftKnee]] = -theta2_mo+mid1_angle2;//0.524087;//
  d->qpos[q_id[RightHipPitch]]=-theta1_mo+mid1_angle1;//RHP
  d->qpos[q_id[RightKnee]] = theta2_mo-mid1_angle2;



  
}
