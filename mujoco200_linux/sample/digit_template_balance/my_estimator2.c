
//initialize the estimator here
double dist_com[3][1];
double base_angvel2[3][1];
void init_estimator()
{

}


//estimation in this loop
void estimate_loop(llapi_observation_t* observation)
{
	double base_pos[3] = {observation->base.translation[0],observation->base.translation[1],observation->base.translation[2]};
	double base_quat[4] = {observation->base.orientation[3],observation->base.orientation[0],observation->base.orientation[1],observation->base.orientation[2]};
	double com_pos[3] = {observation->robot.com_pos[0],observation->robot.com_pos[1],observation->robot.com_pos[2]};
	struct digit_joint_pos joint_pos;
	joint_pos.LHY = observation->motor.position[LeftHipYaw];
	joint_pos.LHR = observation->motor.position[LeftHipRoll];
	joint_pos.LHP = observation->motor.position[LeftHipPitch];
	joint_pos.LK = observation->motor.position[LeftKnee];
	joint_pos.LS = observation->joint.position[LeftShin];
	joint_pos.LT = observation->joint.position[LeftTarsus];
	joint_pos.LTP = observation->joint.position[LeftToePitch];
	joint_pos.LTR = observation->joint.position[LeftToeRoll];
	get_com_footL(base_pos, base_quat, com_pos, joint_pos, tree, dist_com);

	double base_angvel[3];

	base_angvel[0]=observation->base.angular_velocity[0];
    base_angvel[1]=observation->base.angular_velocity[1];
    base_angvel[2]=observation->base.angular_velocity[2];

    //double base_angvel2[3][1];
    double base_angvel_world[3][1]={{base_angvel[0]},{base_angvel[1]},{base_angvel[2]}};
    get_base_angvel_wrt_footL(base_pos, base_quat, joint_pos, tree, base_angvel_world, base_angvel2);



}
