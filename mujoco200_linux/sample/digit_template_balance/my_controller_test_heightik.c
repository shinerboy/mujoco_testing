double setpt[nact]={0};
int upper_body_motors[8]={LeftShoulderRoll,
                                    LeftShoulderPitch,
                                    LeftShoulderYaw,
                                    LeftElbow,

                                    RightShoulderRoll,
                                    RightShoulderPitch,
                                    RightShoulderYaw,
                                    RightElbow,};
double msd;
double tf = 3;
double t_ctr = 0;
char stance_leg ='r';
double th0[3][1]={0};

double theta0;
double theta1;
double theta2;
double theta3;
double theta4;
double Ltoe_pitch;
double Rtoe_pitch;
double tf;
double Lp0[4][1]={0};
double Lpf[4][1]={0};
double Vp0[3][1]={0};
double Vpf[3][1]={0};
double Ap0[3][1]={0};
double Apf[3][1]={0};
double Lp0_2[4][1]={0};
double ss[4][1]={0};

double vv[3][1]={0}; 
double aa[3][1]={0};
double base_H[4][4]={0};
bool conv1 = true; //convergence of ik solver
bool conv2 = true; //convergence of ik solver
double th_Lfeet[2][1];
  double th_Rfeet[2][1];

//initialize the controller here
void init_controller()
{
  ss[3][0]=1;
  tf=3;
  int i;
  double target[]={20*3.14/180,0,0,20*3.14/180, //left leg
                   0,0,
                  -20*3.14/180,0,0,-20*3.14/180, //right leg
                  0,0,
                   0,0,0,0,  //left hand
                   0,0,0,0};  //right hand

  for (i=0;i<nact;i++)
  {
    setpt[i]=target[i];
  }
}

//control in this loop
void control_loop(const llapi_observation_t* observation, llapi_command_t* command)
{
  
  msd = observation->time;
  // printf("msd: %f \n",msd);
  // printf("tf: %f \n",tf);
  //printf("time: %f \n", msd);;
  /*theta0 = 0;
  theta1 = theta1_mo-observation->motor.position[LeftHipPitch];
  theta2 = theta1_mo+observation->motor.position[RightHipPitch];
  theta3 = theta2_mo+observation->motor.position[LeftKnee];
  theta4 = theta2_mo-observation->motor.position[RightKnee];*/

  //command->motors[LeftHipYaw].torque = -1000*(observation->motor.position[LeftHipYaw])-(10*observation->motor.velocity[LeftHipYaw]);
  //command->motors[RightHipYaw].torque = -1000*(observation->motor.position[RightHipYaw])-(10*observation->motor.velocity[RightHipYaw]);

  base_H[0][0]=d->xmat[mj_name2id(m, mjOBJ_XBODY, "base")*9];
  base_H[0][1]=d->xmat[mj_name2id(m, mjOBJ_XBODY, "base")*9+1];
  base_H[0][2]=d->xmat[mj_name2id(m, mjOBJ_XBODY, "base")*9+2];
  base_H[0][3]=d->xpos[mj_name2id(m, mjOBJ_XBODY, "base")*3];
  base_H[1][0]=d->xmat[mj_name2id(m, mjOBJ_XBODY, "base")*9+3];
  base_H[1][1]=d->xmat[mj_name2id(m, mjOBJ_XBODY, "base")*9+4];
  base_H[1][2]=d->xmat[mj_name2id(m, mjOBJ_XBODY, "base")*9+5];
  base_H[1][3]=d->xpos[mj_name2id(m, mjOBJ_XBODY, "base")*3+1];;
  base_H[2][0]=d->xmat[mj_name2id(m, mjOBJ_XBODY, "base")*9+6];
  base_H[2][1]=d->xmat[mj_name2id(m, mjOBJ_XBODY, "base")*9+7];
  base_H[2][2]=d->xmat[mj_name2id(m, mjOBJ_XBODY, "base")*9+8];
  base_H[2][3]=d->xpos[mj_name2id(m, mjOBJ_XBODY, "base")*3+2];
  base_H[3][0]=0;
  base_H[3][1]=0;
  base_H[3][2]=0;
  base_H[3][3]=1;
  //matPrint(4,4,base_H);

  //d->xpos[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*3] = 0.06206;
  //d->xpos[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*3+1] = 0.40260321;
  //d->xpos[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*3+2] = 0.8;

  // d->xquat[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*4] = 1;
  // d->xquat[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*4+1] = 0;
  // d->xquat[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*4+2] = 0;
  // d->xquat[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*4+3] = 0;

  // d->xvel[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*3] = 0;
  // d->xvel[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*3+1] = 0;
  // d->xvel[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*3+2] = 0;


  for (int i=0;i<8;i++)
  {
    command->motors[upper_body_motors[i]].torque = -100*(observation->motor.position[upper_body_motors[i]]+setpt[upper_body_motors[i]])-(1*observation->motor.velocity[upper_body_motors[i]]);

  }

  d->qpos[base_joint_adr+0]=0;
  d->qpos[base_joint_adr+1]=0;
  d->qpos[base_joint_adr+2]=1.2;//1.08;//1.065;
  d->qpos[base_joint_adr+3]=1;//0.9238795;//0.9659258;//0.9990482;//w
  d->qpos[base_joint_adr+4]=0;//0.258819;//0.0864101;//0;
  d->qpos[base_joint_adr+5]=0;//0;
  d->qpos[base_joint_adr+6]=0;//-0.3826834;//0.1300295;//0.258819;//0.0436194;
  d->qvel[base_joint_adr]=0;
  d->qvel[base_joint_adr+1]=0;
  d->qvel[base_joint_adr+2]=0;


  
 /* parallel_toe(&theta0, &theta1, &theta3, &Ltoe_pitch);
  parallel_toe(&theta0, &theta2, &theta4, &Rtoe_pitch);*/

/*  printf("base_x: %f \n",d->xpos[mj_name2id(m, mjOBJ_XBODY, "base")*3]);
  printf("base_y: %f \n",d->xpos[mj_name2id(m, mjOBJ_XBODY, "base")*3+1]);
  printf("base_z: %f \n",d->xpos[mj_name2id(m, mjOBJ_XBODY, "base")*3+2]);*/
  
  if (msd<tf){
  //d->qpos[base_joint_adr+0]=0;
  //d->qpos[base_joint_adr+1]=0;
  //d->qpos[base_joint_adr+2]=1.2;//1.08;//1.065;
  d->qpos[base_joint_adr+3]=1;//0.9238795;//0.9659258;//0.9990482;//w
  d->qpos[base_joint_adr+4]=0;//0.258819;//0.0864101;//0;
  d->qpos[base_joint_adr+5]=0;//0;
  d->qpos[base_joint_adr+6]=0;//-0.3826834;//0.1300295;//0.258819;//0.0436194;
  d->qvel[base_joint_adr]=0;
  d->qvel[base_joint_adr+1]=0;
  //d->qvel[base_joint_adr+2]=0;

   

    for (int i=0;i<12;i++)
  {
    command->motors[i].torque = -100*(observation->motor.position[i]-setpt[i])-(1*observation->motor.velocity[i]);

  }

  
  newtonik_FlatFoot('r',d,m,observation,th_Lfeet,conv2);
  matPrint(2,1,th_Lfeet);
  if(conv2 == false){
        //safety_toggle=true;
    }

  command->motors[LeftToeA].torque = -10*(th_Lfeet[0][0]-observation->joint.position[LeftToePitch])+1*observation->joint.velocity[LeftToePitch]+10*(th_Lfeet[1][0]-observation->joint.position[LeftToeRoll])-1*observation->joint.velocity[LeftToeRoll];
  command->motors[LeftToeB].torque = 10*(th_Lfeet[0][0]-observation->joint.position[LeftToePitch])-1*observation->joint.velocity[LeftToePitch]+10*(th_Lfeet[1][0]-observation->joint.position[LeftToeRoll])-1*observation->joint.velocity[LeftToeRoll];

  
  newtonik_FlatFoot('l',d,m,observation,th_Rfeet,conv2);
  if(conv2 == false){
        //safety_toggle=true;
    }
    matPrint(2,1,th_Rfeet);

  command->motors[RightToeA].torque = -10*(th_Rfeet[0][0]-observation->joint.position[RightToePitch])+1*observation->joint.velocity[RightToePitch]+10*(th_Rfeet[1][0]-observation->joint.position[RightToeRoll])-1*observation->joint.velocity[RightToeRoll];
  command->motors[RightToeB].torque = 10*(th_Rfeet[0][0]-observation->joint.position[RightToePitch])-1*observation->joint.velocity[RightToePitch]+10*(th_Rfeet[1][0]-observation->joint.position[RightToeRoll])-1*observation->joint.velocity[RightToeRoll];

  /*Vp0[0][0]=d->xvel[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*3];
  Vp0[1][0]=d->xvel[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*3+1];
  Vp0[2][0]=d->xvel[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*3+2];*/

  t_ctr=msd;
       

    }

    else{


      
      if(msd<=t_ctr+tf+5 ){
        printf("A \n");
        d->qpos[base_joint_adr+3]=1;//0.9238795;//0.9659258;//0.9990482;//w
        d->qpos[base_joint_adr+4]=0;//0.258819;//0.0864101;//0;
        d->qpos[base_joint_adr+5]=0;//0;
        d->qpos[base_joint_adr+6]=0;//-0.3826834;//0.1300295;//0.258819;//0.0436194;
        d->qvel[base_joint_adr]=0;
        d->qvel[base_joint_adr+1]=0;

        double th_height;

        newtonik_Height('l',d,m,0.6,&th_height,conv2);
        setpt[3]=th_height;
        printf("th_Lheight: %f \n",th_height);
        printf("LTP_position: %f \n",observation->joint.position[LeftToePitch]);
        printf("LTR_position: %f \n",observation->joint.position[LeftToeRoll]);
        printf("LHP_position: %f \n",observation->motor.position[LeftHipPitch]);
        printf("LHR_position: %f \n",observation->motor.position[LeftHipRoll]);
        printf("LK_position: %f \n",observation->motor.position[LeftKnee]);
        printf("basez_position: %f \n",observation->base.translation[2]);

        double F_RW[4][4];

        F_RW[0][0]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "left-foot")*9];
        F_RW[0][1]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "left-foot")*9+1];
        F_RW[0][2]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "left-foot")*9+2];
        F_RW[3][0]=0;

        F_RW[1][0]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "left-foot")*9+3];
        F_RW[1][1]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "left-foot")*9+4];
        F_RW[1][2]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "left-foot")*9+5];
        F_RW[3][1]=0;

        F_RW[2][0]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "left-foot")*9+6];
        F_RW[2][1]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "left-foot")*9+7];
        F_RW[2][2]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "left-foot")*9+8];
        F_RW[3][2]=0;

        F_RW[0][3]=d->xpos[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*3]; //Use the position of LFR because this is the pivot point of the foot
        F_RW[1][3]=d->xpos[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*3+1];
        F_RW[2][3]=d->xpos[mj_name2id(m, mjOBJ_XBODY, "left-toe-roll")*3+2];
        F_RW[3][3]=1;

        double B_RW[4][4];

        B_RW[0][0]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "body")*9];
        B_RW[0][1]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "body")*9+1];
        B_RW[0][2]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "body")*9+2];
        B_RW[3][0]=0;

        B_RW[1][0]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "body")*9+3];
        B_RW[1][1]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "body")*9+4];
        B_RW[1][2]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "body")*9+5];
        B_RW[3][1]=0;

        B_RW[2][0]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "body")*9+6];
        B_RW[2][1]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "body")*9+7];
        B_RW[2][2]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "body")*9+8];
        B_RW[3][2]=0;

        B_RW[0][3]=d->xpos[mj_name2id(m, mjOBJ_XBODY, "body")*3]; //Use the position of LBR because this is the pivot point of the foot
        B_RW[1][3]=d->xpos[mj_name2id(m, mjOBJ_XBODY, "body")*3+1];
        B_RW[2][3]=d->xpos[mj_name2id(m, mjOBJ_XBODY, "body")*3+2];
        B_RW[3][3]=1;

        double invF_RW[4][4];
        matInv2(4,F_RW,invF_RW);
        double F_RB[4][4];
        matMult(4,4,4,4,invF_RW,B_RW,F_RB);
        printf("Base position wrt LF: \n");
        matPrint(4,4,F_RB);

        /////--------------------------------------------------------------------------------------
        newtonik_Height('r',d,m,0.8,&th_height,conv2);
        setpt[9]=th_height;
        printf("th_Rheight: %f \n",th_height);
        printf("RTP_position: %f \n",observation->joint.position[RightToePitch]);
        printf("RTR_position: %f \n",observation->joint.position[RightToeRoll]);
        printf("RHP_position: %f \n",observation->motor.position[RightHipPitch]);
        printf("RHR_position: %f \n",observation->motor.position[RightHipRoll]);
        printf("RK_position: %f \n",observation->motor.position[RightKnee]);
        printf("basez_position: %f \n",observation->base.translation[2]);

        F_RW[0][0]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "right-foot")*9];
        F_RW[0][1]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "right-foot")*9+1];
        F_RW[0][2]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "right-foot")*9+2];
        F_RW[3][0]=0;

        F_RW[1][0]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "right-foot")*9+3];
        F_RW[1][1]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "right-foot")*9+4];
        F_RW[1][2]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "right-foot")*9+5];
        F_RW[3][1]=0;

        F_RW[2][0]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "right-foot")*9+6];
        F_RW[2][1]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "right-foot")*9+7];
        F_RW[2][2]=d->site_xmat[mj_name2id(m, mjOBJ_SITE, "right-foot")*9+8];
        F_RW[3][2]=0;

        F_RW[0][3]=d->xpos[mj_name2id(m, mjOBJ_XBODY, "right-toe-roll")*3]; //Use the position of LFR because this is the pivot point of the foot
        F_RW[1][3]=d->xpos[mj_name2id(m, mjOBJ_XBODY, "right-toe-roll")*3+1];
        F_RW[2][3]=d->xpos[mj_name2id(m, mjOBJ_XBODY, "right-toe-roll")*3+2];
        F_RW[3][3]=1;

        B_RW[3][3]=1;

        //double invF_RW[4][4];
        matInv2(4,F_RW,invF_RW);
        //double F_RB[4][4];
        matMult(4,4,4,4,invF_RW,B_RW,F_RB);
        printf("Base position wrt RF: \n");
        matPrint(4,4,F_RB);


        // while(1){

        // }

           for (int i=0;i<12;i++)
            {
              command->motors[i].torque = -100*(observation->motor.position[i]-setpt[i])-(10*observation->motor.velocity[i]);
            }

            // newtonik_FlatFoot('r',d,m,th_Lfeet,conv2);
            // matPrint(2,1,th_Lfeet);
            // if(conv2 == false){
            //       //safety_toggle=true;
            //   }

            command->motors[LeftToeA].torque = -10*(th_Lfeet[0][0]-observation->joint.position[LeftToePitch])+1*observation->joint.velocity[LeftToePitch]+10*(th_Lfeet[1][0]-observation->joint.position[LeftToeRoll])-1*observation->joint.velocity[LeftToeRoll];
            command->motors[LeftToeB].torque = 10*(th_Lfeet[0][0]-observation->joint.position[LeftToePitch])-1*observation->joint.velocity[LeftToePitch]+10*(th_Lfeet[1][0]-observation->joint.position[LeftToeRoll])-1*observation->joint.velocity[LeftToeRoll];

            // double th_Rfeet[2][1];
            // newtonik_FlatFoot('l',d,m,th_Rfeet,conv2);
            // if(conv2 == false){
            //       //safety_toggle=true;
            //   }
            //   matPrint(2,1,th_Rfeet);

            command->motors[RightToeA].torque = -10*(th_Rfeet[0][0]-observation->joint.position[RightToePitch])+1*observation->joint.velocity[RightToePitch]+10*(th_Rfeet[1][0]-observation->joint.position[RightToeRoll])-1*observation->joint.velocity[RightToeRoll];
            command->motors[RightToeB].torque = 10*(th_Rfeet[0][0]-observation->joint.position[RightToePitch])-1*observation->joint.velocity[RightToePitch]+10*(th_Rfeet[1][0]-observation->joint.position[RightToeRoll])-1*observation->joint.velocity[RightToeRoll];


        }

      else if(msd>t_ctr+tf && msd <= t_ctr+2*tf ){

       }
     
    }
}

  

