#define nact 20
#define njnt 10
#define nbase 13
double gearing[] ={80,50,16,16,  //left top leg
                   50,50,        //left bottom leg
                   80,50,16,16,  //right top leg
                   50,50,       //right bottom leg
                   80,80,50,80, //left hand
                   80,80,50,80};//right hand

double cmd_min[]={-1.4,-1.4,-12.5,-12.5,
                  -10,-10,//-0.9,-0.9,
                  -1.4,-1.4,-12.5,-12.5,
                  -0.9,-0.9,
                  -1.4,-1.4,-1.4,-1.4,
                  -1.4,-1.4,-1.4,-1.4
                  };

double cmd_max[]={1.4,1.4,12.5,12.5,
                  10,10,//0.9,0.9,
                  1.4,1.4,12.5,12.5,
                  0.9,0.9,
                  1.4,1.4,1.4,1.4,
                  1.4,1.4,1.4,1.4
                  };
