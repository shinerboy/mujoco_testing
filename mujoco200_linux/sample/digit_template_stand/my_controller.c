double setpt[nact]={0};

//initialize the controller here
void init_controller()
{
  int i;
  double target[]={0.3752*0,0.2,0.3,-0.75, //left leg
                   0,0,
                  -0.3752*0,-0.2,-0.3,0.75, //right leg
                  0,0,
                   0,0,0,0,  //left hand
                   0,0,0,0};  //right hand

  for (i=0;i<nact;i++)
  {
    setpt[i]=target[i];
  }
}

//control in this loop
void control_loop(const llapi_observation_t* observation, llapi_command_t* command)
{
  int i;
  for (i=0;i<nact;i++)
  {
  command->motors[i].torque = -150*(observation->motor.position[i]-setpt[i])-20*observation->motor.velocity[i];
  }
}
