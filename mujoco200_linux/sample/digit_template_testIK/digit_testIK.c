/*  Copyright © 2018, Roboti LLC

    This file is licensed under the MuJoCo Resource License (the "License").
    You may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        https://www.roboti.us/resourcelicense.txt
*/

//#include <iostream>
#include<stdbool.h>

#include "mujoco.h"
#include "glfw3.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "../../lib/AR_MJ_lowlevelapi.h"

#include "libtest.h"
#include "matram.h"
#include "controller_walk.h"
#include "traj.h"
#include "get_params_walk.h"
#include "parallel_toe.h"

#include "getcom.h"
#include "getphaseangles.h"
#include "quaternion/Quaternion.h"
#include "quaternion/eul2rotm.h"
#include "quaternion/rotm2eul.h"
#include "digit_tree.h"
#include "digit_store_joint_positions.h"
#include "dist_com_foot.h"
#include "base_angvel_foot.h"

#include "digit_balance.h"





// #include <gsl/gsl_matrix.h>
// #include <gsl/gsl_blas.h>
// #include <gsl/gsl_matrix_double.h>
// #include <gsl/gsl_linalg.h>
//#include "useful.c"

//THis file is not to be touched by the user
int q_id[nact]={0}, v_id[nact]={0}, act_id[nact]={0};
int q_jid[njnt]={0}, v_jid[njnt]={0};
int base_stateid[nbase]={0};
int sensor_com[3]={0};
int base_joint_adr;
double t_sim_end = 6; //end time for simulation



// MuJoCo data structures
mjModel* m = NULL;                  // MuJoCo model
mjData* d = NULL;                   // MuJoCo data
mjvCamera cam;                      // abstract camera
mjvOption opt;                      // visualization options
mjvScene scn;                       // abstract scene
mjrContext con;                     // custom GPU context

// mouse interaction
bool button_left = false;
bool button_middle = false;
bool button_right =  false;
double lastx = 0;
double lasty = 0;
#include "my_estimator2.c"
#include "setup.c"
#include "my_controller2.c"


llapi_command_t command = {0};
    llapi_observation_t observation;




// keyboard callback
void keyboard(GLFWwindow* window, int key, int scancode, int act, int mods)
{
    // backspace: reset simulation
    if( act==GLFW_PRESS && key==GLFW_KEY_BACKSPACE )
    {
        mj_resetData(m, d);
        mj_forward(m, d);
    }
}


// mouse button callback
void mouse_button(GLFWwindow* window, int button, int act, int mods)
{
    // update button state
    button_left =   (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT)==GLFW_PRESS);
    button_middle = (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_MIDDLE)==GLFW_PRESS);
    button_right =  (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT)==GLFW_PRESS);

    // update mouse position
    glfwGetCursorPos(window, &lastx, &lasty);
}


// mouse move callback
void mouse_move(GLFWwindow* window, double xpos, double ypos)
{
    // no buttons down: nothing to do
    if( !button_left && !button_middle && !button_right )
        return;

    // compute mouse displacement, save
    double dx = xpos - lastx;
    double dy = ypos - lasty;
    lastx = xpos;
    lasty = ypos;

    // get current window size
    int width, height;
    glfwGetWindowSize(window, &width, &height);

    // get shift key state
    bool mod_shift = (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT)==GLFW_PRESS ||
                      glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT)==GLFW_PRESS);

    // determine action based on mouse button
    mjtMouse action;
    if( button_right )
        action = mod_shift ? mjMOUSE_MOVE_H : mjMOUSE_MOVE_V;
    else if( button_left )
        action = mod_shift ? mjMOUSE_ROTATE_H : mjMOUSE_ROTATE_V;
    else
        action = mjMOUSE_ZOOM;

    // move camera
    mjv_moveCamera(m, action, dx/height, dy/height, &scn, &cam);
}


// scroll callback
void scroll(GLFWwindow* window, double xoffset, double yoffset)
{
    // emulate vertical mouse motion = 5% of window height
    mjv_moveCamera(m, mjMOUSE_ZOOM, 0, -0.05*yoffset, &scn, &cam);
}

// initialize the different ids
void get_ID()
{
  const char* joint_name;
  int bodyid,i;

  joint_name = "left-shoulder-roll";
  bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
  // printf("body id = %d \n",bodyid);
  // printf("*************\n");
  int L_shoulder_roll_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
  int L_shoulder_roll_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];
  int L_shoulder_roll_actuatorID = mj_name2id(m, mjOBJ_ACTUATOR, joint_name);

  joint_name = "left-shoulder-pitch";
  bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
  int L_shoulder_pitch_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
  int L_shoulder_pitch_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];
  int L_shoulder_pitch_actuatorID = mj_name2id(m, mjOBJ_ACTUATOR, joint_name);

  joint_name = "left-shoulder-yaw";
  bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
  int L_shoulder_yaw_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
  int L_shoulder_yaw_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];
  int L_shoulder_yaw_actuatorID = mj_name2id(m, mjOBJ_ACTUATOR, joint_name);

  joint_name = "left-elbow";
  bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
  int L_elbow_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
  int L_elbow_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];
  int L_elbow_actuatorID = mj_name2id(m, mjOBJ_ACTUATOR, joint_name);

  joint_name = "left-hip-roll";
  bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
  int L_hip_roll_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
  int L_hip_roll_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];
  int L_hip_roll_actuatorID = mj_name2id(m, mjOBJ_ACTUATOR, joint_name);

  joint_name = "left-hip-yaw";
  bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
  int L_hip_yaw_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
  int L_hip_yaw_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];
  int L_hip_yaw_actuatorID = mj_name2id(m, mjOBJ_ACTUATOR, joint_name);

  joint_name = "left-hip-pitch";
  bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
  int L_hip_pitch_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
  int L_hip_pitch_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];
  int L_hip_pitch_actuatorID = mj_name2id(m, mjOBJ_ACTUATOR, joint_name);

  joint_name = "left-knee";
  bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
  int L_knee_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
  int L_knee_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];
  int L_knee_actuatorID = mj_name2id(m, mjOBJ_ACTUATOR, joint_name);

  joint_name = "right-shoulder-roll";
bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
int R_shoulder_roll_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
int R_shoulder_roll_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];
int R_shoulder_roll_actuatorID = mj_name2id(m, mjOBJ_ACTUATOR, joint_name);

joint_name = "right-shoulder-pitch";
bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
int R_shoulder_pitch_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
int R_shoulder_pitch_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];
int R_shoulder_pitch_actuatorID = mj_name2id(m, mjOBJ_ACTUATOR, joint_name);

joint_name = "right-shoulder-yaw";
bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
int R_shoulder_yaw_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
int R_shoulder_yaw_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];
int R_shoulder_yaw_actuatorID = mj_name2id(m, mjOBJ_ACTUATOR, joint_name);

joint_name = "right-elbow";
bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
int R_elbow_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
int R_elbow_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];
int R_elbow_actuatorID = mj_name2id(m, mjOBJ_ACTUATOR, joint_name);

joint_name = "right-hip-roll";
bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
int R_hip_roll_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
int R_hip_roll_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];
int R_hip_roll_actuatorID = mj_name2id(m, mjOBJ_ACTUATOR, joint_name);

joint_name = "right-hip-yaw";
bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
int R_hip_yaw_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
int R_hip_yaw_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];
int R_hip_yaw_actuatorID = mj_name2id(m, mjOBJ_ACTUATOR, joint_name);

joint_name = "right-hip-pitch";
bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
int R_hip_pitch_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
int R_hip_pitch_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];
int R_hip_pitch_actuatorID = mj_name2id(m, mjOBJ_ACTUATOR, joint_name);

joint_name = "right-knee";
bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
int R_knee_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
int R_knee_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];
int R_knee_actuatorID = mj_name2id(m, mjOBJ_ACTUATOR, joint_name);

joint_name = "left-toe-A";
bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
int L_toeA_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
int L_toeA_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];
int L_toeA_actuatorID = mj_name2id(m, mjOBJ_ACTUATOR, joint_name);

joint_name = "left-toe-B";
bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
int L_toeB_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
int L_toeB_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];
int L_toeB_actuatorID = mj_name2id(m, mjOBJ_ACTUATOR, joint_name);

joint_name = "right-toe-A";
bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
int R_toeA_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
int R_toeA_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];
int R_toeA_actuatorID = mj_name2id(m, mjOBJ_ACTUATOR, joint_name);

joint_name = "right-toe-B";
bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
int R_toeB_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
int R_toeB_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];
int R_toeB_actuatorID = mj_name2id(m, mjOBJ_ACTUATOR, joint_name);



   int temp_IDq[] = {L_hip_roll_qpos_adr,L_hip_yaw_qpos_adr,L_hip_pitch_qpos_adr,L_knee_qpos_adr,
               L_toeA_qpos_adr,L_toeB_qpos_adr,
                R_hip_roll_qpos_adr,R_hip_yaw_qpos_adr,R_hip_pitch_qpos_adr,R_knee_qpos_adr,
                R_toeA_qpos_adr,R_toeB_qpos_adr,
               L_shoulder_roll_qpos_adr,L_shoulder_pitch_qpos_adr,L_shoulder_yaw_qpos_adr,L_elbow_qpos_adr,
              R_shoulder_roll_qpos_adr,R_shoulder_pitch_qpos_adr,R_shoulder_yaw_qpos_adr,R_elbow_qpos_adr};

   int temp_IDv[] = {L_hip_roll_qvel_adr,L_hip_yaw_qvel_adr,L_hip_pitch_qvel_adr,L_knee_qvel_adr,
                L_toeA_qvel_adr,L_toeB_qvel_adr,
                R_hip_roll_qvel_adr,R_hip_yaw_qvel_adr,R_hip_pitch_qvel_adr,R_knee_qvel_adr,
                R_toeA_qvel_adr,R_toeB_qvel_adr,
                L_shoulder_roll_qvel_adr,L_shoulder_pitch_qvel_adr,L_shoulder_yaw_qvel_adr,L_elbow_qvel_adr,
                R_shoulder_roll_qvel_adr,R_shoulder_pitch_qvel_adr,R_shoulder_yaw_qvel_adr,R_elbow_qvel_adr};
   int temp_IDT[] = {L_hip_roll_actuatorID,L_hip_yaw_actuatorID,L_hip_pitch_actuatorID,L_knee_actuatorID,
                L_toeA_actuatorID,L_toeB_actuatorID,
                R_hip_roll_actuatorID,R_hip_yaw_actuatorID,R_hip_pitch_actuatorID,R_knee_actuatorID,
                R_toeA_actuatorID,R_toeB_actuatorID,
                L_shoulder_roll_actuatorID,L_shoulder_pitch_actuatorID,L_shoulder_yaw_actuatorID,L_elbow_actuatorID,
               R_shoulder_roll_actuatorID,R_shoulder_pitch_actuatorID,R_shoulder_yaw_actuatorID,R_elbow_actuatorID};


   for (i=0;i<nact;i++)
     {
       q_id[i] = temp_IDq[i];
       v_id[i] = temp_IDv[i];
       act_id[i] = temp_IDT[i];
    }

joint_name = "left-shin";
bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
int L_shin_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
int L_shin_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];

joint_name = "left-tarsus";
bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
int L_tarsus_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
int L_tarsus_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];

joint_name = "left-toe-pitch";
bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
int L_toe_pitch_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
int L_toe_pitch_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];

joint_name = "left-toe-roll";
bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
int L_toe_roll_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
int L_toe_roll_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];

joint_name = "left-heel-spring";
bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
int L_heel_spring_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
int L_heel_spring_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];



joint_name = "right-shin";
bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
int R_shin_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
int R_shin_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];

joint_name = "right-tarsus";
bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
int R_tarsus_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
int R_tarsus_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];

joint_name = "right-toe-pitch";
bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
int R_toe_pitch_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
int R_toe_pitch_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];

joint_name = "right-toe-roll";
bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
int R_toe_roll_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
int R_toe_roll_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];

joint_name = "right-heel-spring";
bodyid = mj_name2id(m, mjOBJ_BODY, joint_name);
int R_heel_spring_qpos_adr = m->jnt_qposadr[m->body_jntadr[bodyid]];
int R_heel_spring_qvel_adr = m->jnt_dofadr[m->body_jntadr[bodyid]];

int temp_JIDq[] = {L_shin_qpos_adr,L_tarsus_qpos_adr,L_toe_pitch_qpos_adr,L_toe_roll_qpos_adr, L_heel_spring_qpos_adr,
                R_shin_qpos_adr,R_tarsus_qpos_adr,R_toe_pitch_qpos_adr,R_toe_roll_qpos_adr, R_heel_spring_qpos_adr};

int temp_JIDv[] = {L_shin_qvel_adr,L_tarsus_qvel_adr,L_toe_pitch_qvel_adr,L_toe_roll_qvel_adr, L_heel_spring_qvel_adr,
                R_shin_qvel_adr,R_tarsus_qvel_adr,R_toe_pitch_qvel_adr,R_toe_roll_qvel_adr, R_heel_spring_qvel_adr};
for (i=0;i<njnt;i++)
     {
       q_jid[i] = temp_JIDq[i];
       v_jid[i] = temp_JIDv[i];
    }
//Base
const char* base_pos_name = "base-pos";
int base_sensorID = mj_name2id(m, mjOBJ_SENSOR, base_pos_name);
int base_sensor_adr = m->sensor_adr[base_sensorID];
const char* base_quat_name = "base-quat";
int base_quat_sensorID = mj_name2id(m, mjOBJ_SENSOR, base_quat_name);
int base_quat_sensor_adr = m->sensor_adr[base_quat_sensorID];


int base_linvel_sensorID = mj_name2id(m, mjOBJ_SENSOR, "base-linvel");
int base_linvel_sensor_adr = m->sensor_adr[base_linvel_sensorID];
int base_angvel_sensorID = mj_name2id(m, mjOBJ_SENSOR, "base-angvel");
int base_angvel_sensor_adr = m->sensor_adr[base_angvel_sensorID];


int temp_baseID[] = {base_sensor_adr, base_sensor_adr+1, base_sensor_adr+2,
                        base_quat_sensor_adr+1, base_quat_sensor_adr+2, base_quat_sensor_adr+3, base_quat_sensor_adr, //w component should be last
                        base_linvel_sensor_adr, base_linvel_sensor_adr+1, base_linvel_sensor_adr+2,
                        base_angvel_sensor_adr, base_angvel_sensor_adr+1, base_angvel_sensor_adr+2}; 
for (i=0;i<nbase;i++)
     {
       base_stateid[i] = temp_baseID[i];
    }
//COM sensor
const char* com_sensor = "com-sensor";
int com_sensorID = mj_name2id(m, mjOBJ_SENSOR, com_sensor);
int com_sensor_adr = m->sensor_adr[com_sensorID];
sensor_com[0] = com_sensor_adr;
sensor_com[1] = com_sensor_adr+1;
sensor_com[2] = com_sensor_adr+2;

const char* base_joint_name = "base";
        int base_jointID = mj_name2id(m, mjOBJ_JOINT, base_joint_name);
        base_joint_adr = m->jnt_qposadr[base_jointID];

  printf("\n\n ************ All joint IDs set ***** \n\n");

}

//get states from the robot or from the simulator
void get_state(llapi_observation_t* observation)
{
  int i;
  observation->time = d->time;
  for (i=0;i<nact;i++)
  {
    // q[i] = d->qpos[q_id[i]];
    // u[i] = d->qvel[v_id[i]];
    observation->motor.position[i] = d->qpos[q_id[i]];
    observation->motor.velocity[i] = d->qvel[v_id[i]];
    //printf("%f \n",observation->motor.position[i]);
  }
  for (i=0;i<njnt;i++)
  {
    observation->joint.position[i] = d->qpos[q_jid[i]];
    observation->joint.velocity[i] = d->qvel[v_jid[i]];
    
  }
  for (i=0;i<nbase;i++)
  {
    if (i<=2){
        observation->base.translation[i] = d->sensordata[base_stateid[i]];
    }
    else if (i>2 && i <=6){
        observation->base.orientation[i-3] = d->sensordata[base_stateid[i]];
    }
    else if (i>6 && i<=9){
        observation->base.linear_velocity[i-7] = d->sensordata[base_stateid[i]];
    }
    else if (i>9 && i<=12){
        observation->base.angular_velocity[i-10] = d->sensordata[base_stateid[i]];
    } 
  }

  for (i=0;i<3;i++)
  {
    observation->robot.com_pos[i]=d->sensordata[sensor_com[i]];
    
  }

}


//check limits before sending it to the controller
void clamp_command(llapi_command_t* command)
{
  int i;
  double cmd;
  for (i=0;i<nact;i++)
  {
     //cmd = command->motors[i].torque/gearing[i];
     cmd = command->motors[i].torque;
     //printf("i: %d \n",i);
     //printf("cmd: %f \n",cmd);
  if (cmd<(cmd_min[i]*gearing[i]))
    {cmd = cmd_min[i]*gearing[i];
        printf("too low: %d \n",i);}
  else if (cmd>(cmd_max[i]*gearing[i]))
    {cmd = cmd_max[i]*gearing[i];
        printf("too high: %d \n",i);}

    d->ctrl[act_id[i]] = cmd;

  }
}

// control loop callback
void loop(const mjModel* m, mjData* d)
{

  // int i;
  // double tau[nact]={0};
  // double q[nact]={0},u[nact]={0};

  get_state(&observation); //get state from the simulator or robot
  

  estimate_loop(&observation); //if there is state estimation
  control_loop(&observation,&command); //control loop
  clamp_command(&command); //clamp the torque
  //printf("IN LOOP \n");

 }

// main function
int main(int argc, const char** argv)
{
    

    // check command-line arguments
    if( argc!=2 )
    {
        printf(" USAGE:  basic modelfile\n");
        return 0;
    }

    // activate software
    mj_activate("../../../mjkey.txt");

    // load and compile model
    char error[1000] = "Could not load binary model";
    if( strlen(argv[1])>4 && !strcmp(argv[1]+strlen(argv[1])-4, ".mjb") )
        m = mj_loadModel(argv[1], 0);
    else
        m = mj_loadXML(argv[1], 0, error, 1000);
    if( !m )
        mju_error_s("Load model error: %s", error);

    // make data
    d = mj_makeData(m);


    // init GLFW
    if( !glfwInit() )
        mju_error("Could not initialize GLFW");

    // create window, make OpenGL context current, request v-sync
    GLFWwindow* window = glfwCreateWindow(1200, 900, "Demo", NULL, NULL);//1200,900
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);

    // initialize visualization data structures
    mjv_defaultCamera(&cam);
    mjv_defaultOption(&opt);
    mjv_defaultScene(&scn);
    mjr_defaultContext(&con);

    // create scene and context
    mjv_makeScene(m, &scn, 2000);
    mjr_makeContext(m, &con, mjFONTSCALE_150);

    // install GLFW mouse and keyboard callbacks
    glfwSetKeyCallback(window, keyboard);
    glfwSetCursorPosCallback(window, mouse_move);
    glfwSetMouseButtonCallback(window, mouse_button);
    glfwSetScrollCallback(window, scroll);

    //get IDs of joints considered
    get_ID();

    // install control callback
    mjcb_control = loop;

    //set camera distance
    //double arr_view  = {-160,-22.8,4,-0.18,0.2,0.48};
    //double arr_view[] = {-151,-12,4.37,-0.043,-0.05,1.2}; //view the left side (for ll, lh, left_side)
    //double arr_view[] = {-179.000000, -12.800000, 4.370000, -0.043000, -0.050000, 1.200000}; //front view
    double arr_view[] = {-179.000000, 2.600000, 4.370000, -0.043000, -0.050000, 1.200000};


    // cam.azimuth = arr_view[0];
    // cam.elevation = arr_view[1];
    // cam.distance =arr_view[2];
    // cam.lookat[0] = arr_view[3];
    // cam.lookat[1] =arr_view[4];
    // cam.lookat[2] = arr_view[5];

    cam.azimuth = 90;//-160;
    cam.elevation = 0;//-22.8;
    cam.distance = 4;
    cam.lookat[0] = -0.18;
    cam.lookat[1] = 0.2;
    cam.lookat[2] = 0.9;//0.48;
    // run main loop, target real-time simulation and 60 fps rendering

    setup();

    init_estimator();
    init_controller();


    while( !glfwWindowShouldClose(window) )
    {
        // advance interactive simulation for 1/60 sec
        //  Assuming MuJoCo can simulate faster than real-time, which it usually can,
        //  this loop will finish on time for the next frame to be rendered at 60 fps.
        //  Otherwise add a cpu timer and exit this loop when it is time to render.
        //printf("%f \n",d->time);

        mjtNum simstart = d->time;
        while( d->time - simstart < 1.0/60.0 )
        {
        //while( d->time - simstart < 1.0/240.0 ) //increasing from 60 to 240 decreases rendering speed
            mj_step(m, d);
        }

       //if (d->time>=t_sim_end)
          //break;


        // get framebuffer viewport
        mjrRect viewport = {0, 0, 0, 0};
        glfwGetFramebufferSize(window, &viewport.width, &viewport.height);

        // update scene and render
        mjv_updateScene(m, d, &opt, NULL, &cam, mjCAT_ALL, &scn);
        mjr_render(viewport, &scn, &con);

        // swap OpenGL buffers (blocking call due to v-sync)
        glfwSwapBuffers(window);

        // process pending GUI events, call GLFW callbacks
        glfwPollEvents();

        // move the animation to get updated values.
        //printf("{%f, %f, %f, %f, %f, %f};\n",cam.azimuth,cam.elevation, cam.distance,cam.lookat[0],cam.lookat[1],cam.lookat[2]);

    }

    //free visualization storage
    mjv_freeScene(&scn);
    mjr_freeContext(&con);

    // free MuJoCo model and data, deactivate
    mj_deleteData(d);
    mj_deleteModel(m);
    mj_deactivate();

    //sd_end();

    // terminate GLFW (crashes with Linux NVidia drivers)
    #if defined(__APPLE__) || defined(_WIN32)
        glfwTerminate();
    #endif

    return 1;
}
