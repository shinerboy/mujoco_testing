double setpt[nact]={0};

//initialize the controller here
void init_controller()
{
  int i;
  double target[]={0.3752*0,0.2*0,0.3*0,0*-0.75, //left leg
                   0,0,
                  -0.3752*0,-0.2*0,-0.3*0,0*0.75, //right leg
                  0,0,
                   0,0,0,0,  //left hand
                   0,0,0,0};  //right hand

  for (i=0;i<nact;i++)
  {
    setpt[i]=target[i];
  }
}

//control in this loop
void control_loop(const llapi_observation_t* observation, llapi_command_t* command)
{
  unsigned int i;
  for (i=0;i<nact;i++)
    //printf("%d \n", LeftToeA);
  {
    if (i != LeftToeA && i != LeftToeB && i != RightToeA && i != RightToeB){
      //printf("IN IF \n");
      command->motors[i].torque = -150*(observation->motor.position[i]-setpt[i])-20*observation->motor.velocity[i];
      //command->motors[i].torque = 0;
    }
    else {
      // =10;
      //command->motors[LeftToeB].torque =10;
      // d->ctrl[LTB_actuatorID] = LTB_ctrl; 
      double LToePitch_des = -0;
      double LToeRoll_des = -0.2;

      command->motors[LeftToeA].torque = -50.0*(LToePitch_des-observation->joint.position[LeftToePitch])-50.0*(-LToeRoll_des+observation->joint.position[LeftToeRoll]) - 10*observation->joint.velocity[LeftToeRoll] - 10*observation->joint.velocity[LeftToePitch];
      
      command->motors[LeftToeB].torque = 50.0*(LToePitch_des-observation->joint.position[LeftToePitch])+50.0*(LToeRoll_des-observation->joint.position[LeftToeRoll]) - 10*observation->joint.velocity[LeftToeRoll] - 10*observation->joint.velocity[LeftToePitch];
      

      printf("LToePitch observation: %f \n", observation->joint.position[LeftToePitch]);
      printf("LToeRoll observation: %f \n", observation->joint.position[LeftToeRoll]);

      double RToePitch_des = 0.5;
      double RToeRoll_des = -0.2;

      command->motors[RightToeA].torque = -50.0*(RToePitch_des-observation->joint.position[RightToePitch])-50.0*(-RToeRoll_des+observation->joint.position[RightToeRoll]) - 10*observation->joint.velocity[RightToeRoll];
      
      command->motors[RightToeB].torque = 50.0*(RToePitch_des-observation->joint.position[RightToePitch])+50.0*(RToeRoll_des-observation->joint.position[RightToeRoll]) - 10*observation->joint.velocity[RightToeRoll];;
      

      printf("RToePitch observation: %f \n", observation->joint.position[RightToePitch]);
      printf("RToeRoll observation: %f \n", observation->joint.position[RightToeRoll]);

    }
  
  }
}
